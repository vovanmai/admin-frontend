import moment from 'moment'

const lessOrEqualNow = {
  validate (value) {
    if (!value) {
      return true
    }
    value = moment(value).second(0).format('YYYY-MM-DD HH:mm:ss')
    const now = moment().second(0).format('YYYY-MM-DD HH:mm:ss')
    return moment(now).diff(value, 'minutes') >= 0
  }
}
export {
  lessOrEqualNow
}
