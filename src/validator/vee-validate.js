import Vue from 'vue'
import { ValidationProvider, ValidationObserver, localize, extend } from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'

// Register it globally
import en from './locales/en'
import vi from './locales/vi'
import * as customRules from './custom-rules'

Object.keys(customRules).forEach(rule => {
  extend(rule, customRules[rule])
})

export const initValidation = (locale) => {
  Vue.component('ValidationProvider', ValidationProvider)
  Vue.component('ValidationObserver', ValidationObserver)

  Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule])
  })
  // Install English and Viet nam locales.
  localize({
    en,
    vi
  })
  // Set locale for vee-validate
  localize(locale)
}
