import axios from 'axios'
import { isEmpty } from 'lodash'
import store from '@/store'
import router from '@/router'
import storage from 'store'
import { ACCESS_TOKEN } from '@/store/mutation-types'
import toastr from '@/core/toastr'
import { trans } from '@/locales'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

axios.interceptors.request.use(
  (config) => {
    const accessToken = storage.get(ACCESS_TOKEN)

    if (!isEmpty(accessToken)) {
      const headers = {
        Authorization: `Bearer ${accessToken}`
      }

      config.headers = Object.assign(config.headers, headers)
    }

    return config
  },
  error => Promise.reject(error)
)

axios.interceptors.response.use(
  (response) => {
    return response.data
  },
  (error) => {
    const statusCode = error.response && error.response.status
    switch (statusCode) {
      case 401:
        store.dispatch('auth/removeToken')
        router.push({ name: 'login' })
        toastr.error(trans('auth.invalid_token'))
        break
      case 403:
        break
      case 404:
        break
      case 500:
        toastr.error(trans('error.exception_error'))
        break
      default:
        break
    }
    return Promise.reject(error.response && error.response)
  })
