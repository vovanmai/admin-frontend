import HttpRequest from '../HttpRequest'

class ProductRequest extends HttpRequest {
  list (params) {
    return this.get('products', params)
  }

  create (data) {
    return this.post('products', data)
  }

  show (id) {
    return this.get(`products/${id}`)
  }

  update (id, data) {
    return this.put(`products/${id}`, data)
  }
}

export default new ProductRequest()
