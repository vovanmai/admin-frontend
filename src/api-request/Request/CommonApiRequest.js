import HttpRequest from '../HttpRequest'

class CommonApiRequest extends HttpRequest {
  getSelectProducts (params) {
    return this.get('products/all', params)
  }

  getUnits () {
    return this.get('units')
  }

  getAvailableTables () {
    return this.get('tables/available')
  }

  getSelectProductUnits () {
    return this.get('product_units/all')
  }
}

export default new CommonApiRequest()
