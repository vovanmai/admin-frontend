import HttpRequest from '../HttpRequest'

class ProductUnitRequest extends HttpRequest {
  list (params) {
    return this.get('product-units', params)
  }

  create (data) {
    return this.post('product-units', data)
  }
}

export default new ProductUnitRequest()
