import HttpRequest from '../HttpRequest'

class AuthRequest extends HttpRequest {
  login (data) {
    return this.post('authenticate', data)
  }

  me () {
    return this.get('me')
  }

  logout () {
    return this.get('logout')
  }
}

export default new AuthRequest()
