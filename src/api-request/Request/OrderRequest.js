import HttpRequest from '../HttpRequest'

class OrderRequest extends HttpRequest {
  list (params) {
    return this.get('orders', params)
  }

  create (data) {
    return this.post('orders', data)
  }

  show (id) {
    return this.get(`orders/${id}`)
  }

  calculateCheckout (data) {
    return this.post(`orders/calculate-checkout`, data)
  }

  update (id, data) {
    return this.put(`orders/${id}`, data)
  }
}

export default new OrderRequest()
