import HttpRequest from '../HttpRequest'

class TableRequest extends HttpRequest {
  list (params) {
    return this.get('tables', params)
  }

  statistic (params) {
    return this.get('tables/statistic', params)
  }

  create (data) {
    return this.post('tables', data)
  }

  show (id) {
    return this.get(`tables/${id}`)
  }

  status (params) {
    return this.get('tables/status', params)
  }
}

export default new TableRequest()
