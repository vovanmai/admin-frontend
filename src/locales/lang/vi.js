import antdEnUS from 'ant-design-vue/es/locale-provider/vi_VN'
import momentEU from 'moment/locale/vi'
import global from './vi-VN/global'

import menu from './vi-VN/menu'
import setting from './vi-VN/setting'
import user from './vi-VN/user'

import dashboard from './vi-VN/dashboard'
import form from './vi-VN/form'
import result from './vi-VN/result'
import account from './vi-VN/account'
import message from './vi-VN/message'

const components = {
  antLocale: antdEnUS,
  momentName: 'eu',
  momentLocale: momentEU
}

export default {
  'layouts.usermenu.dialog.title': 'Thông báo',
  'layouts.usermenu.dialog.content': 'Bạn có chắc chắn đăng xuất không?',
  'layouts.userLayout.title': 'Ant Design is the most influential web design specification in Xihu district',
  ...components,
  ...global,
  ...menu,
  ...setting,
  ...user,
  ...dashboard,
  ...form,
  ...result,
  ...account,
  ...message
}
