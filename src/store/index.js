import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
import auth from './modules/auth'
import table from './modules/table'
import product from './modules/product'
import productUnit from './modules/productUnit'
import commonApi from './modules/commonApi'
import order from './modules/order'

// default router permission control
import permission from './modules/permission'

// dynamic router permission control (Experimental)
// import permission from './modules/async-router'
import getters from './getters'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    user,
    permission,
    auth,
    table,
    productUnit,
    commonApi,
    order,
    product
  },
  state: {

  },
  mutations: {

  },
  actions: {

  },
  getters
})
