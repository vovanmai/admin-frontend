import ProductRequest from '@/api-request/Request/ProductRequest'

const table = {
  namespaced: true,
  state: {
    items: [],
    item: {},
    currentPage: 1,
    total: 0
  },

  mutations: {
    SET_ITEMS: (state, items) => {
      items.forEach(function (item, index) {
        item.index = index + 1
      })
      state.items = items
    },
    SET_CURRENT_PAGE: (state, data) => {
      state.currentPage = data
    },
    SET_TOTAL: (state, data) => {
      state.total = data
    },
    SET_ITEM: (state, data) => {
      state.item = data
    }
  },

  actions: {
    async list ({ commit }, params) {
      try {
        const response = await ProductRequest.list(params)
        commit('SET_ITEMS', response.data.items)
        commit('SET_CURRENT_PAGE', response.data.current_page)
        commit('SET_TOTAL', response.data.total)
        return response
      } catch (e) {
        throw e
      }
    },

    async create ({ commit }, data) {
      try {
        const response = await ProductRequest.create(data)
        return response
      } catch (e) {
        throw e
      }
    },

    async update ({ commit }, data) {
      try {
        const response = await ProductRequest.update(data.id, data.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async show ({ commit }, id) {
      try {
        const response = await ProductRequest.show(id)
        commit('SET_ITEM', response.data)
        return response.data
      } catch (e) {
        throw e
      }
    }
  }
}

export default table
