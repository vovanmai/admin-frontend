import OrderRequest from '@/api-request/Request/OrderRequest'

const table = {
  namespaced: true,
  state: {
    items: [],
    item: {},
    currentPage: 1,
    total: 0,
    calculatedOrder: {}
  },

  mutations: {
    SET_ITEMS: (state, items) => {
      items.forEach(function (item, index) {
        item.index = index + 1
      })
      state.items = items
    },
    SET_CURRENT_PAGE: (state, data) => {
      state.currentPage = data
    },
    SET_TOTAL: (state, data) => {
      state.total = data
    },
    SET_ITEM: (state, data) => {
      state.item = data
    },
    SET_CALCULATED_ORDER: (state, data) => {
      state.calculatedOrder = data
    }
  },

  actions: {
    async list ({ commit }, params) {
      try {
        const response = await OrderRequest.list(params)
        commit('SET_ITEMS', response.data.items)
        commit('SET_CURRENT_PAGE', response.data.current_page)
        commit('SET_TOTAL', response.data.total)
        return response
      } catch (e) {
        throw e
      }
    },

    async create ({ commit }, data) {
      try {
        const response = await OrderRequest.create(data)
        return response
      } catch (e) {
        throw e
      }
    },

    async update ({ commit }, data) {
      try {
        const response = await OrderRequest.update(data.id, data.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async validateCheckout ({ commit }, data) {
      try {
        const response = await OrderRequest.validateCheckout(data.id, data.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async show ({ commit }, id) {
      try {
        const response = await OrderRequest.show(id)
        commit('SET_ITEM', response.data)
        return response.data
      } catch (e) {
        throw e
      }
    },

    async calculateCheckout ({ commit }, data) {
      try {
        const response = await OrderRequest.calculateCheckout(data)
        commit('SET_CALCULATED_ORDER', response.data)
        return response.data
      } catch (e) {
        throw e
      }
    }
  }
}

export default table
