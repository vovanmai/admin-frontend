import CommonApiRequest from '@/api-request/Request/CommonApiRequest'

const table = {
  namespaced: true,
  state: {
    selectProducts: [],
    units: [],
    availableTables: [],
    productUnits: []
  },

  mutations: {
    SET_SELECT_PRODUCTS: (state, items) => {
      state.selectProducts = items
    },
    SET_UNITS: (state, items) => {
      state.units = items
    },
    SET_AVAILABLE_TABLES: (state, items) => {
      state.availableTables = items
    },
    SET_PRODUCT_UNITS: (state, items) => {
      state.productUnits = items
    }
  },

  actions: {
    async getSelectProducts ({ commit }, params) {
      try {
        const response = await CommonApiRequest.getSelectProducts(params)
        commit('SET_SELECT_PRODUCTS', response.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async getUnits ({ commit }) {
      try {
        const response = await CommonApiRequest.getUnits()
        commit('SET_UNITS', response.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async getAvailableTables ({ commit }) {
      try {
        const response = await CommonApiRequest.getAvailableTables()
        commit('SET_AVAILABLE_TABLES', response.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async getSelectProductUnits ({ commit }) {
      try {
        const response = await CommonApiRequest.getSelectProductUnits()
        commit('SET_PRODUCT_UNITS', response.data)
        return response
      } catch (e) {
        throw e
      }
    }
  }
}

export default table
