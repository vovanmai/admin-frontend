import AuthRequest from '@/api-request/Request/AuthRequest'
import { ACCESS_TOKEN } from '@/store/mutation-types'
import storage from 'store'

const auth = {
  namespaced: true,
  state: {
    role: null,
    name: null,
    avatar: null,
    info: {}
  },

  mutations: {
    SET_ROLE: (state, role) => {
      state.role = role
    },
    SET_NAME: (state, data) => {
      state.name = data
    },
    SET_INFO: (state, info) => {
      state.info = info
    },
    LOGIN_SUCCESS: (state, payload) => {
      state.role = payload.role
      state.name = payload.name
      state.info = payload
    },
    LOGOUT_SUCCESS: (state, payload) => {
      state.role = null
      state.info = {}
    }
  },

  actions: {
    async login ({ commit }, data) {
      try {
        const response = await AuthRequest.login(data)
        commit('LOGIN_SUCCESS', response.data.user_info)
        storage.set(ACCESS_TOKEN, response.data.token)
        return response
      } catch (e) {
        throw e
      }
    },
    async me ({ commit }) {
      try {
        const response = await AuthRequest.me()
        commit('SET_ROLE', response.data.role)
        commit('SET_NAME', response.data.name)
        return response
      } catch (e) {
        throw e
      }
    },
    async logout ({ commit }) {
      try {
        await AuthRequest.logout()
        commit('LOGOUT_SUCCESS')
        storage.remove(ACCESS_TOKEN)
      } catch (e) {
        throw e
      }
    },
    removeToken ({ commit }) {
      commit('LOGOUT_SUCCESS')
      storage.remove(ACCESS_TOKEN)
    }
  }
}

export default auth
