import ProductUnitRequest from '@/api-request/Request/ProductUnitRequest'

const table = {
  namespaced: true,
  state: {
    items: [],
    currentPage: 1,
    total: 0
  },

  mutations: {
    SET_ITEMS: (state, items) => {
      items.forEach(function (item, index) {
        item.index = index + 1
      })
      state.items = items
    },
    SET_CURRENT_PAGE: (state, data) => {
      state.currentPage = data
    },
    SET_TOTAL: (state, data) => {
      state.total = data
    }
  },

  actions: {
    async list ({ commit }, params) {
      try {
        const response = await ProductUnitRequest.list(params)
        commit('SET_ITEMS', response.data.items)
        commit('SET_CURRENT_PAGE', response.data.current_page)
        commit('SET_TOTAL', response.data.total)
        return response
      } catch (e) {
        throw e
      }
    },

    async create ({ commit }, data) {
      try {
        const response = await ProductUnitRequest.create(data)
        return response
      } catch (e) {
        throw e
      }
    }
  }
}

export default table
