import TableRequest from '@/api-request/Request/TableRequest'

const table = {
  namespaced: true,
  state: {
    items: [],
    item: {},
    currentPage: 1,
    total: 0,
    statusItems: [],
    statisticData: {}
  },

  mutations: {
    SET_ITEMS: (state, items) => {
      items.forEach(function (item, index) {
        item.index = index + 1
      })
      state.items = items
    },
    SET_ITEM: (state, item) => {
      state.item = item
    },
    SET_CURRENT_PAGE: (state, data) => {
      state.currentPage = data
    },
    SET_TOTAL: (state, data) => {
      state.total = data
    },
    SET_STATUS_ITEMS: (state, data) => {
      state.statusItems = data
    },
    SET_STATISTIC: (state, data) => {
      state.statisticData = data
    }
  },

  actions: {
    async list ({ commit }, params) {
      try {
        const response = await TableRequest.list(params)
        commit('SET_ITEMS', response.data.items)
        commit('SET_CURRENT_PAGE', response.data.current_page)
        commit('SET_TOTAL', response.data.total)
        return response
      } catch (e) {
        throw e
      }
    },

    async status ({ commit }, params) {
      try {
        const response = await TableRequest.status(params)
        commit('SET_STATUS_ITEMS', response.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async statistic ({ commit }) {
      try {
        const response = await TableRequest.statistic()
        commit('SET_STATISTIC', response.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async show ({ commit }, id) {
      try {
        const response = await TableRequest.show(id)
        commit('SET_ITEM', response.data)
        return response
      } catch (e) {
        throw e
      }
    },

    async create ({ commit }, data) {
      try {
        const response = await TableRequest.create(data)
        return response
      } catch (e) {
        throw e
      }
    },

    updateStatistic ({ commit }, data) {
      commit('SET_STATISTIC', data)
    }
  }
}

export default table
