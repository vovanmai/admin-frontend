import _ from 'lodash'

export default class Helper {
  static filterObject (object) {
    const filters = ['', null, undefined]
    for (const propName in object) {
      if (!filters.indexOf(_.trim(object[propName]))) {
        delete object[propName]
      }
    }
    return object
  }

  static removeDuplicate (array) {
    return [...new Set(array)]
  }
}
