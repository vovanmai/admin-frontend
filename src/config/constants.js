const API_URL = process.env.VUE_APP_BASE_API_URL || 'http://localhost'

const DEFAULT_LANGUAGE = 'vi'

const REQUEST_HEADER = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
  'X-Requested-With': 'XMLHttpRequest'
}

const WEBSITE_NAME = process.env.VUE_APP_WEBSITE_NAME || 'My Application'

const SUPPER_ADMIN = 1
const ADMIN = 2

const TIME_ZONE = process.env.VUE_APP_TIME_ZONE || 'Asia/Ho_Chi_Minh'

export {
  REQUEST_HEADER,
  DEFAULT_LANGUAGE,
  API_URL,
  WEBSITE_NAME,
  SUPPER_ADMIN,
  ADMIN,
  TIME_ZONE
}
