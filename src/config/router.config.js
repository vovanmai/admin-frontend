import { UserLayout, BasicLayout } from '@/layouts'
// import { bxAnaalyse } from '@/core/icons'
import { SUPPER_ADMIN, ADMIN } from './../config/constants'

const RouteView = {
  name: 'RouteView',
  render: h => h('router-view')
}

export const asyncRouterMap = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: 'menu.home' },
    redirect: '/dashboard/workplace',
    children: [
      // dashboard
      {
        path: '/dashboard',
        name: 'dashboard',
        redirect: '/dashboard/workplace',
        component: RouteView,
        meta: { title: 'menu.dashboard', keepAlive: true, icon: 'dashboard', permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/dashboard/analysis/:pageNo([1-9]\\d*)?',
            name: 'Analysis',
            component: () => import('@/views/dashboard/Analysis'),
            meta: { title: 'menu.dashboard.analysis', keepAlive: false, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/dashboard/workplace',
            name: 'Workplace',
            component: () => import('@/views/dashboard/Workplace'),
            meta: { title: 'menu.dashboard.workplace', keepAlive: true, permission: [ SUPPER_ADMIN ] }
          }
        ]
      },
      {
        path: '/table-management',
        name: 'table-management',
        redirect: '/table-management/tables/list',
        component: RouteView,
        meta: { title: 'menu.table.management', keepAlive: true, icon: 'table', permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/table-management/tables',
            name: 'tables',
            redirect: '/table-management/tables/list',
            component: RouteView,
            meta: {
              title: 'menu.table.list',
              keepAlive: false,
              permission: [ SUPPER_ADMIN ]
            },
            hideChildrenInMenu: true,
            children: [
              {
                path: '/table-management/tables/list',
                name: 'list-table',
                component: () => import('@/views/table/List'),
                meta: { title: 'menu.list', hideHeader: true, hidden: true, keepAlive: false, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/table-management/tables/create',
                name: 'create-table',
                component: () => import('@/views/table/Create'),
                meta: { title: 'menu.create', keepAlive: false, permission: [ SUPPER_ADMIN ] }
              }
            ]
          },
          {
            path: '/table-management/tables/status',
            name: 'table-status',
            component: () => import('@/views/table/Status'),
            meta: { title: 'menu.table.status', keepAlive: false, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/table-management/tables/orders',
            name: 'orders',
            redirect: '/table-management/tables/orders/list',
            component: RouteView,
            meta: {
              title: 'order.list',
              keepAlive: false,
              permission: [ SUPPER_ADMIN ]
            },
            hideChildrenInMenu: true,
            children: [
              {
                path: '/table-management/tables/orders/list',
                name: 'list-order',
                component: () => import('@/views/order/List'),
                meta: { title: 'list', hideHeader: true, hidden: true, keepAlive: false, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/table-management/tables/orders/create',
                name: 'create-order',
                component: () => import('@/views/order/Create'),
                meta: { title: 'list', hideHeader: true, hidden: true, keepAlive: false, permission: [ SUPPER_ADMIN ] }
              }
            ]
          }
        ]
      },
      {
        path: '/product-management',
        name: 'product-management',
        redirect: '/product-management/products/list',
        component: RouteView,
        meta: { title: 'menu.product.management', keepAlive: true, icon: 'table', permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/product-management/products',
            name: 'products',
            redirect: '/product-management/products/list',
            component: RouteView,
            meta: {
              title: 'menu.product.list',
              keepAlive: false,
              permission: [ SUPPER_ADMIN ]
            },
            hideChildrenInMenu: true,
            children: [
              {
                path: '/product-management/products/list',
                name: 'list-product',
                component: () => import('@/views/product/List'),
                meta: { title: 'menu.list', hideHeader: true, hidden: true, keepAlive: false, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/product-management/products/create',
                name: 'create-product',
                component: () => import('@/views/product/Create'),
                meta: { title: 'menu.create', keepAlive: false, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/product-management/products/edit/:id',
                name: 'edit-product',
                component: () => import('@/views/product/Edit'),
                meta: { title: 'edit', keepAlive: false, permission: [ SUPPER_ADMIN ] }
              }
            ]
          },
          {
            path: '/product-management/product-units',
            name: 'product-units',
            redirect: '/product-management/product-units/list',
            component: RouteView,
            meta: {
              title: 'menu.product_unit.list',
              keepAlive: false,
              permission: [ SUPPER_ADMIN ]
            },
            hideChildrenInMenu: true,
            children: [
              {
                path: '/product-management/product-units/list',
                name: 'list-product-unit',
                component: () => import('@/views/product-unit/List'),
                meta: { title: 'menu.list', hideHeader: true, hidden: true, keepAlive: false, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/product-management/product-units/create',
                name: 'create-product-unit',
                component: () => import('@/views/product-unit/Create'),
                meta: { title: 'menu.create', keepAlive: false, permission: [ SUPPER_ADMIN ] }
              }
            ]
          }
        ]
      },
      // forms
      {
        path: '/form',
        redirect: '/form/base-form',
        component: RouteView,
        meta: { title: 'menu.form', icon: 'form', permission: [ SUPPER_ADMIN, ADMIN ] },
        children: [
          {
            path: '/form/base-form',
            name: 'BaseForm',
            component: () => import('@/views/form/basicForm'),
            meta: { title: 'menu.form.basic-form', keepAlive: true, hidden: true, permission: [ SUPPER_ADMIN, ADMIN ] }
          },
          {
            path: '/form/step-form',
            name: 'StepForm',
            component: () => import('@/views/form/stepForm/StepForm'),
            meta: { title: 'menu.form.step-form', keepAlive: true, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/form/advanced-form',
            name: 'AdvanceForm',
            component: () => import('@/views/form/advancedForm/AdvancedForm'),
            meta: { title: 'menu.form.advanced-form', keepAlive: true, permission: [ SUPPER_ADMIN ] }
          }
        ]
      },
      // list
      {
        path: '/list',
        name: 'list',
        component: RouteView,
        redirect: '/list/table-list',
        meta: { title: 'menu.list', icon: 'table', permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/list/table-list/:pageNo([1-9]\\d*)?',
            name: 'TableListWrapper',
            hideChildrenInMenu: true, // 强制显示 MenuItem 而不是 SubMenu
            component: () => import('@/views/list/TableList'),
            meta: { title: 'menu.list.table-list', keepAlive: true, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/list/basic-list',
            name: 'BasicList',
            component: () => import('@/views/list/BasicList'),
            meta: { title: 'menu.list.basic-list', keepAlive: true, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/list/card',
            name: 'CardList',
            component: () => import('@/views/list/CardList'),
            meta: { title: 'menu.list.card-list', keepAlive: true, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/list/search',
            name: 'SearchList',
            component: () => import('@/views/list/search/SearchLayout'),
            redirect: '/list/search/article',
            meta: { title: 'menu.list.search-list', keepAlive: true, permission: [ SUPPER_ADMIN ] },
            children: [
              {
                path: '/list/search/article',
                name: 'SearchArticles',
                component: () => import('../views/list/search/Article'),
                meta: { title: 'menu.list.search-list.articles', permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/list/search/project',
                name: 'SearchProjects',
                component: () => import('../views/list/search/Projects'),
                meta: { title: 'menu.list.search-list.projects', permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/list/search/application',
                name: 'SearchApplications',
                component: () => import('../views/list/search/Applications'),
                meta: { title: 'menu.list.search-list.applications', permission: [ SUPPER_ADMIN ] }
              }
            ]
          }
        ]
      },

      // profile
      {
        path: '/profile',
        name: 'profile',
        component: RouteView,
        redirect: '/profile/basic',
        meta: { title: 'menu.profile', icon: 'profile', permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/profile/basic',
            name: 'ProfileBasic',
            component: () => import('@/views/profile/basic'),
            meta: { title: 'menu.profile.basic', permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/profile/advanced',
            name: 'ProfileAdvanced',
            component: () => import('@/views/profile/advanced/Advanced'),
            meta: { title: 'menu.profile.advanced', permission: [ SUPPER_ADMIN ] }
          }
        ]
      },

      // result
      {
        path: '/result',
        name: 'result',
        component: RouteView,
        redirect: '/result/success',
        meta: { title: 'menu.result', icon: 'check-circle-o', permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/result/success',
            name: 'ResultSuccess',
            component: () => import(/* webpackChunkName: "result" */ '@/views/result/Success'),
            meta: { title: 'menu.result.success', keepAlive: false, hiddenHeaderContent: true, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/result/fail',
            name: 'ResultFail',
            component: () => import(/* webpackChunkName: "result" */ '@/views/result/Error'),
            meta: { title: 'menu.result.fail', keepAlive: false, hiddenHeaderContent: true, permission: [ SUPPER_ADMIN ] }
          }
        ]
      },

      // Exception
      {
        path: '/exception',
        name: 'exception',
        component: RouteView,
        redirect: '/exception/403',
        meta: { title: 'menu.exception', icon: 'warning', permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/exception/403',
            name: 'Exception403',
            component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403'),
            meta: { title: 'menu.exception.not-permission', permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/exception/404',
            name: 'Exception404',
            component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404'),
            meta: { title: 'menu.exception.not-find', permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/exception/500',
            name: 'Exception500',
            component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500'),
            meta: { title: 'menu.exception.server-error', permission: [ SUPPER_ADMIN ] }
          }
        ]
      },

      // account
      {
        path: '/account',
        component: RouteView,
        redirect: '/account/center',
        name: 'account',
        meta: { title: 'menu.account', icon: 'user', keepAlive: true, permission: [ SUPPER_ADMIN ] },
        children: [
          {
            path: '/account/center',
            name: 'center',
            component: () => import('@/views/account/center'),
            meta: { title: 'menu.account.center', keepAlive: true, permission: [ SUPPER_ADMIN ] }
          },
          {
            path: '/account/settings',
            name: 'settings',
            component: () => import('@/views/account/settings/Index'),
            meta: { title: 'menu.account.settings', hideHeader: true, permission: [ SUPPER_ADMIN ] },
            redirect: '/account/settings/basic',
            hideChildrenInMenu: true,
            children: [
              {
                path: '/account/settings/basic',
                name: 'BasicSettings',
                component: () => import('@/views/account/settings/BasicSetting'),
                meta: { title: 'account.settings.menuMap.basic', hidden: true, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/account/settings/security',
                name: 'SecuritySettings',
                component: () => import('@/views/account/settings/Security'),
                meta: {
                  title: 'account.settings.menuMap.security',
                  hidden: true,
                  keepAlive: true,
                  permission: [ SUPPER_ADMIN ]
                }
              },
              {
                path: '/account/settings/custom',
                name: 'CustomSettings',
                component: () => import('@/views/account/settings/Custom'),
                meta: { title: 'account.settings.menuMap.custom', hidden: true, keepAlive: true, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/account/settings/binding',
                name: 'BindingSettings',
                component: () => import('@/views/account/settings/Binding'),
                meta: { title: 'account.settings.menuMap.binding', hidden: true, keepAlive: true, permission: [ SUPPER_ADMIN ] }
              },
              {
                path: '/account/settings/notification',
                name: 'NotificationSettings',
                component: () => import('@/views/account/settings/Notification'),
                meta: {
                  title: 'account.settings.menuMap.notification',
                  hidden: true,
                  keepAlive: true,
                  permission: [ SUPPER_ADMIN ]
                }
              }
            ]
          }
        ]
      }

      // other
      /*
      {
        path: '/other',
        name: 'otherPage',
        component: PageView,
        meta: { title: '其他组件', icon: 'slack', permission: [ 'dashboard' ] },
        redirect: '/other/icon-selector',
        children: [
          {
            path: '/other/icon-selector',
            name: 'TestIconSelect',
            component: () => import('@/views/other/IconSelectorView'),
            meta: { title: 'IconSelector', icon: 'tool', keepAlive: true, permission: [ 'dashboard' ] }
          },
          {
            path: '/other/list',
            component: RouteView,
            meta: { title: '业务布局', icon: 'layout', permission: [ 'support' ] },
            redirect: '/other/list/tree-list',
            children: [
              {
                path: '/other/list/tree-list',
                name: 'TreeList',
                component: () => import('@/views/other/TreeList'),
                meta: { title: '树目录表格', keepAlive: true }
              },
              {
                path: '/other/list/edit-table',
                name: 'EditList',
                component: () => import('@/views/other/TableInnerEditList'),
                meta: { title: '内联编辑表格', keepAlive: true }
              },
              {
                path: '/other/list/user-list',
                name: 'UserList',
                component: () => import('@/views/other/UserList'),
                meta: { title: '用户列表', keepAlive: true }
              },
              {
                path: '/other/list/role-list',
                name: 'RoleList',
                component: () => import('@/views/other/RoleList'),
                meta: { title: '角色列表', keepAlive: true }
              },
              {
                path: '/other/list/system-role',
                name: 'SystemRole',
                component: () => import('@/views/role/RoleList'),
                meta: { title: '角色列表2', keepAlive: true }
              },
              {
                path: '/other/list/permission-list',
                name: 'PermissionList',
                component: () => import('@/views/other/PermissionList'),
                meta: { title: '权限列表', keepAlive: true }
              }
            ]
          }
        ]
      }
      */
    ]
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

export const constantRouterMap = [
  {
    path: '/',
    component: UserLayout,
    redirect: '/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import('@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import('@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import('@/views/user/RegisterResult')
      },
      {
        path: 'recover',
        name: 'recover',
        component: undefined
      }
    ]
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]
