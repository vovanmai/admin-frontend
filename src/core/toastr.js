import toastr from 'toastr'
import { trans } from '../locales'

export default class Toast {
  static init (preventDuplicate = true) {
    toastr.options.preventDuplicates = true
  }

  static success (message, title = trans('toastr.success')) {
    this.init()

    toastr.success(message, title)
  }

  static info (message, title = trans('toastr.info')) {
    this.init()
    toastr.info(message, title)
  }

  static warning (message, title = trans('toastr.warning')) {
    this.init()
    toastr.warning(message, title)
  }

  static error (message, title = trans('toastr.error')) {
    this.init()
    toastr.error(message, title)
  }
}
